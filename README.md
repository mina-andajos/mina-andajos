# Mina Andajos
<a href="https://www.linkedin.com/in/mina-andajos-9b89101b2/"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"></a>
<a href="mailto:mina-andajos-work@outlook.com"><img src="https://img.shields.io/badge/Microsoft_Outlook-0078D4?style=for-the-badge&logo=microsoft-outlook&logoColor=white"></a>
<a href="https://discordapp.com/channels/@me/747449468864954438/"><img src="https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white"></a>


Accepted as an Intern for the [Outreachy May 2022 Cohort](https://www.outreachy.org)

Egyptian Engineering Student. Building projects with Django and aspiring to be a Full-Stack Web developer.

I ❤️ GNU/Linux, Dracula Theme and Charles Hoskinson.

---------------

<a href="https://github.com/anuraghazra/github-readme-stats">
  <img align="left" src="https://github-readme-stats.vercel.app/api?username=mina-andajos&show_icons=true&include_all_commits=true&theme=dracula" alt="Anurag's github stats" />
</a>
<a href="https://github.com/anuraghazra/github-readme-stats">
  <img align="left" src="https://github-readme-stats.vercel.app/api/top-langs/?username=mina-andajos&layout=compact&theme=dracula" />
</a>
